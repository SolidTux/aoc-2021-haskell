module Day01 where

generate :: String -> [Int]
generate = map read . lines

run01p1 = calc . generate

run01p2 = calc . movsum . generate

calc :: [Int] -> Int
calc [] = 0
calc [x] = 0
calc (x : xs) = (if x < head xs then 1 else 0) + calc xs

movsum [x, y] = []
movsum (a : b : xs) = (a + b + head xs) : movsum (b : xs)
movsum _ = []