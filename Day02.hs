module Day02 where

import Data.List
import Data.Maybe
import Debug.Trace

data Movement = Forward | Up | Down deriving (Show)

instance Read Movement where
  readsPrec _ s = case map (`stripPrefix` s) ["forward", "up", "down"] of
    [Just s, _, _] -> [(Forward, s)]
    [Nothing, Just s, _] -> [(Up, s)]
    [Nothing, Nothing, Just s] -> [(Down, s)]
    _ -> []

parsemov s = (read a, read b)
  where
    v = words s
    a = head v
    b = head . tail $ v

generate :: String -> [(Movement, Int)]
generate = map parsemov . lines

move (x, y) (Forward, z) = (x + z, y)
move (x, y) (Up, z) = (x, y - z)
move (x, y) (Down, z) = (x, y + z)

pos :: [(Movement, Int)] -> (Int, Int)
pos = foldl move (0, 0)

run02p1 = uncurry (*) . pos . generate

move2 (x, y, a) (Forward, z) = (x + z, y + z * a, a)
move2 (x, y, a) (Up, z) = (x, y, a - z)
move2 (x, y, a) (Down, z) = (x, y, a + z)

pos2 l = (a, b)
  where
    (a, b, _) = foldl move2 (0, 0, 0) l

run02p2 = uncurry (*) . pos2 . generate