{-# LANGUAGE TupleSections #-}

module Day03 where

singleton x = [x]

tobool '1' = True
tobool _ = False

frombool True = '1'
frombool False = '0'

generate :: String -> [[Bool]]
generate = map (map tobool) . lines

gamma = conv . map comp

epsilon = conv . map (not . comp)

run03p1 x = gamma c * epsilon c
  where
    c = common . generate $ x

common :: [[Bool]] -> [(Int, Int)]
common l = common_ x l
  where
    x = replicate (length . head $ l) (0, 0)

comp (a, b) = b >= a

conv :: [Bool] -> Int
conv = sum . map ((2 ^) . fst) . filter snd . zip [0 ..] . reverse

common_ :: [(Int, Int)] -> [[Bool]] -> [(Int, Int)]
common_ x [] = x
common_ x (y : ys) = common_ z ys
  where
    z = zipWith acc x y

acc (a, b) False = (a + 1, b)
acc (a, b) True = (a, b + 1)

rating :: (Bool -> Bool) -> [[Bool]] -> Int
rating f = rating_ f . map ([],)

rating_ :: (Bool -> Bool) -> [([Bool], [Bool])] -> Int
rating_ _ [] = 0
rating_ _ [(a, b)] = conv (a ++ b)
rating_ f l = rating_ f . map (\(a, b, c) -> (a ++ [b], c)) . filter (\(_, a, _) -> a == x) $ l2
  where
    l2 = map (\(a, b) -> (a, head b, tail b)) l
    x = f . comp . head . common . map (\(_, a, _) -> [a]) $ l2

oxygen = rating id

co2 = rating not

run03p2 y = oxygen x * co2 x
  where
    x = generate y
