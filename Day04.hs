{-# LANGUAGE TupleSections #-}

module Day04 where

import Data.List
import Data.Maybe
import Debug.Trace
import Text.Printf (printf)

type Row = [(Int, Bool)]

type Board = [Row]

disp = unlines . map disprow

disprow = unwords . map dispcell

dispcell :: (Int, Bool) -> String
dispcell (x, True) = printf "\ESC[31;1m%4d\ESC[0m" x
dispcell (x, False) = printf "%4d" x

split :: Char -> String -> [String]
split c xs = case break (== c) xs of
  (ls, "") -> [ls]
  (ls, x : rs) -> ls : split c rs

generate :: String -> ([Int], [Board])
generate x = (v, boards [] l)
  where
    v = map read . split ',' . head . lines $ x
    l = tail . tail . lines $ x

boards :: [Row] -> [String] -> [Board]
boards b [] = [b]
boards b ("" : xs) = b : boards [] xs
boards b (x : xs) = boards (b ++ [map ((,False) . read) . words $x]) xs

run04p1 :: String -> Int
run04p1 = uncurry score . uncurry (winning 0) . generate

score :: Int -> Board -> Int
score x = (*) x . sum . map fst . filter (not . snd) . concat

winning :: Int -> [Int] -> [Board] -> (Int, Board)
winning _ [] _ = error "No winning board found"
winning z (x : xs) b = case find iswin b of
  Just y -> (z, y)
  Nothing -> winning x xs . map (mark x) $ b

iswin :: Board -> Bool
iswin b = iswinrow b || (iswinrow . transpose $ b)

iswinrow :: Board -> Bool
iswinrow = isJust . find (all snd)

mark :: Int -> Board -> Board
mark x = map (markrow x)

markrow :: Int -> Row -> Row
markrow x = map (markcell x)

markcell x (y, v) = (y, v || x == y)

run04p2 :: String -> Int
run04p2 = uncurry score . uncurry (lastwinning 0) . generate

lastwinning :: Int -> [Int] -> [Board] -> (Int, Board)
lastwinning z (x : xs) [b] = if iswin b then (z, b) else lastwinning x xs [mark x b]
lastwinning z (x : xs) b = lastwinning x xs . filter (not . iswin) . map (mark x) $ b
lastwinning _ _ _ = error "No winning board found"