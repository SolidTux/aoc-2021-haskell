module Day05 where

import Data.Map (Map, empty, insertWith, toList)
import Data.Ord
import Debug.Trace

type Area = (Point, Point)

type Point = (Int, Int)

split :: Char -> String -> [String]
split c xs = case break (== c) xs of
  (ls, "") -> [ls]
  (ls, x : rs) -> ls : split c rs

strtopoint s = (a, b)
  where
    p = split ',' s
    a = read . head $ p
    b = read . head . tail $ p

strtoarea s = (a, b)
  where
    p = words s
    a = strtopoint (head p)
    b = strtopoint (p !! 2)

generate :: String -> [Area]
generate = map strtoarea . lines

ishv :: Area -> Bool
ishv ((x0, y0), (x1, y1)) = x0 == x1 || y0 == y1

points :: Area -> [Point]
points ((x0, y0), (x1, y1)) = zip [x0, x0 + dx2 .. x1] [y0, y0 + dy2 .. y1]
  where
    dx = x1 - x0
    dy = y1 - y0
    d = gcd dx dy
    dx2 = div dx d
    dy2 = div dy d

run05p1 :: String -> Int
run05p1 = length . filter ((1 <) . snd) . toList . overlaps empty . concatMap points . filter ishv . generate

run05p2 :: String -> Int
run05p2 = length . filter ((1 <) . snd) . toList . overlaps empty . concatMap points . generate

overlaps :: Map Point Int -> [Point] -> Map Point Int
overlaps = foldl (\m x -> insertWith (+) x 1 m)