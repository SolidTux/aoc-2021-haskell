module Day06 where

import Data.List
import Data.Map.Strict (Map, elems, empty, fromList, insertWith, keys, mapMaybe, mapWithKey, toList, (!))
import Debug.Trace

type Fish = Int

split :: Char -> String -> [String]
split c xs = case break (== c) xs of
  (ls, "") -> [ls]
  (ls, x : rs) -> ls : split c rs

generate :: String -> Map Fish Int
generate = foldl (\x y -> insertWith (+) y 1 x) empty . map read . split ','

fishstep :: Fish -> [Fish]
fishstep 0 = [6, 8]
fishstep x = [x - 1]

addnewfish :: [Fish] -> Int -> Map Fish Int -> Map Fish Int
addnewfish [] _ m = m
addnewfish (x : xs) b m = insertWith (+) x b . addnewfish xs b $ m

addfish :: Map Fish Int -> (Fish, Int) -> Map Fish Int
addfish m (x, b) = addnewfish new b m
  where
    new = fishstep x

step :: Map Fish Int -> Map Fish Int
step m = clean $ foldl addfish empty (toList m)

clean :: Map Fish Int -> Map Fish Int
clean = mapMaybe (\x -> if x > 0 then Just x else Nothing)

steps :: Int -> Map Fish Int -> Map Fish Int
steps 0 f = f
steps x f = steps (x - 1) (step f)

calc n = sum . elems . steps n . generate

run06p1 = calc 80

run06p2 = calc 256

disp :: Map Fish Int -> Map Fish Int
disp x = traceShow (sort $ foldl acc [] (keys x)) x
  where
    acc :: [Int] -> Fish -> [Int]
    acc = \l k -> l ++ replicate (x ! k) k
