module Day07 where

import Data.List
import Debug.Trace

split :: Char -> String -> [String]
split c xs = case break (== c) xs of
  (ls, "") -> [ls]
  (ls, x : rs) -> ls : split c rs

generate :: String -> [Int]
generate = map read . split ','

run07p1 :: String -> Int
run07p1 = calc . generate

calc :: [Int] -> Int
calc l = minimum . map (dist l) $ [minimum l .. maximum l]

dist l x = sum . map (abs . (-) x) $ l

run07p2 :: String -> Int
run07p2 = calc2 . generate

calc2 :: [Int] -> Int
calc2 l = minimum . map (dist2 l) $ [minimum l .. maximum l]

dist2 l x = sum . map ((\x -> (x ^ 2 + x) `div` 2) . abs . (-) x) $ l
