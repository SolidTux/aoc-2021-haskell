module Day08 where

import Data.Char (intToDigit)
import Data.List
import Data.List.Extra (stripInfix)
import Data.Map.Strict (Map, elems, empty, fromList, insertWith, keys, mapWithKey, member, toList, (!), (!?))
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Tuple
import Data.Tuple.Extra

generate :: String -> [([String], [String])]
generate = map (both (map sort . words) . fromJust . stripInfix " | ") . lines

run08p1 = length . filter uniqueDigit . concatMap snd . generate

uniqueDigit x = (length <$> (lenMap !? length x)) == Just 1

lenMap :: Map Int [Int]
lenMap = foldl (\m x -> insertWith (++) (length . elements $ x) [x] m) empty [0 .. 9]

elements :: Int -> String
elements 0 = "abcefg"
elements 1 = "cf"
elements 2 = "acdeg"
elements 3 = "acdfg"
elements 4 = "bcdf"
elements 5 = "abdfg"
elements 6 = "abdefg"
elements 7 = "acf"
elements 8 = "abcdefg"
elements 9 = "abcdfg"
elements x = error "invalid digit " ++ show x

fromElements :: String -> Int
fromElements x = head . filter ((==) x . elements) $ [0 .. 9]

maybeFromElements :: String -> Maybe Int
maybeFromElements x = find ((==) x . elements) [0 .. 9]

run08p2 = sum . map solveLine . generate

solveLine :: ([String], [String]) -> Int
solveLine (a, b) = read . map intToDigit . applyMap (genMapBrute a) $ b

applyMap :: Map String Int -> [String] -> [Int]
applyMap m = map (m !)

genMapBrute :: [String] -> Map String Int
genMapBrute l = fromList . map (\x -> (x, fromElements . sort . (<$>) (mc !) $ x)) $ l
  where
    mc = head . filter (testCharMap l) $ charMaps

charMaps :: [Map Char Char]
charMaps = map mapFromStr . permutations $ "abcdefg"

mapFromStr :: String -> Map Char Char
mapFromStr = fromList . zip "abcdefg"

testCharMap :: [String] -> Map Char Char -> Bool
testCharMap l m = (==) 10 . length . mapMaybe (maybeFromElements . sort . ((m !) <$>)) $ l