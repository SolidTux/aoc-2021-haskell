{-# LANGUAGE TupleSections #-}

module Day09 where

import Data.Char (digitToInt, intToDigit)
import Data.List
import Data.Maybe
import Debug.Trace

generate :: String -> [[Int]]
generate = map (map digitToInt) . lines

right = map (\x -> (map Just . tail $ x) ++ [Nothing])

left = map ((:) Nothing . map Just . init)

top x = replicate (length . head $ x) Nothing : (init . map (map Just) $ x)

bottom x = reverse (replicate (length . head $ x) Nothing : (init . reverse . map (map Just) $ x))

maybeLess a (Just b) = a < b
maybeLess a Nothing = True

neighbors x = map (map catMaybes . transpose) . transpose $ [right x, left x, top x, bottom x]

neightborZip a = zip (concat a) . concat . neighbors $ a

minNeighbor :: [[Int]] -> [[Int]]
minNeighbor = map (map minimum) . neighbors

run09p1 = risk . generate

isMin a l = a < minimum l

risk = sum . map ((+) 1 . fst) . filter (uncurry isMin) . neightborZip

run09p2 = product . take 3 . reverse . sort . map length . basins . generate

neighborIndices (x, y) (n, m) = filter (\(x, y) -> x >= 0 && x < n && y >= 0 && y < m) [(x + 1, y), (x - 1, y), (x, y - 1), (x, y + 1)]

ind (x, y) m = m !! y !! x

minInd h p = isMin (ind p h) (map (`ind` h) . neighborIndices p $ size)
  where
    size = (length . head $ h, length h)

basins :: [[Int]] -> [[(Int, Int)]]
basins h = map (basin h) . filter (minInd h) $ indices
  where
    (n, m) = (length . head $ h, length h)
    indices :: [(Int, Int)]
    indices = concatMap (\x -> map (,x) [0 .. n -1]) [0 .. m -1]

basin h p = nub (p : concatMap (basin h) next)
  where
    x = ind p h
    size = (length . head $ h, length h)
    n = neighborIndices p size
    next = filter f n
    f q = y > x && y < 9
      where
        y = ind q h
