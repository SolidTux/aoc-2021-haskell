module Day10 where

import Data.List
import Data.Maybe
import Debug.Trace

data Result = Error Int | Incomplete String | Complete

resScore (Error x) = x
resScore _ = 0

resRem Complete = Just ""
resRem (Incomplete s) = Just s
resRem _ = Nothing

run10p1 = sum . map (resScore . parse []) . lines

score ')' = Just 3
score ']' = Just 57
score '}' = Just 1197
score '>' = Just 25137
score _ = Nothing

close '(' = ')'
close '[' = ']'
close '{' = '}'
close '<' = '>'
close c = error ("Can't close " ++ show c)

parse :: [Char] -> String -> Result
parse "" [] = Complete
parse s [] = Incomplete s
parse [] (x : xs) = case score x of
  Just s -> Error s
  Nothing -> parse [close x] xs
parse (y : ys) (x : xs) =
  if x == y
    then parse ys xs
    else case score x of
      Just s -> Error s
      Nothing -> parse (close x : (y : ys)) xs

score2 ')' = 1
score2 ']' = 2
score2 '}' = 3
score2 '>' = 4
score2 _ = error "Invalid character left"

remScore x "" = x
remScore x (y : ys) = remScore (5 * x + score2 y) ys

median :: [Int] -> Maybe Int
median = median_ . sort

median_ l = case length l of
  0 -> Nothing
  1 -> Just (head l)
  2 -> Nothing
  _ -> median (init . tail $ l)

run10p2 = fromJust . median . map (remScore 0) . mapMaybe (resRem . parse []) . lines