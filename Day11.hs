{-# LANGUAGE TupleSections #-}

module Day11 where

import Data.Char
import Data.List

generate :: String -> [[Int]]
generate = map (map digitToInt) . lines

disp :: [[Int]] -> String
disp = unlines . map (map (intToDigit . min 10))

inc = map (map (1 +))

normalize [] = (0, [])
normalize (x : xs) = (a0 + a1, x0 : x1)
  where
    (a0, x0) = normRow x
    (a1, x1) = normalize xs

normRow [] = (0, [])
normRow (x : xs) = (a0 + a1, x0 : x1)
  where
    (a0, x0) = if x > 9 then (1, 0) else (0, x)
    (a1, x1) = normRow xs

flashing l = filter ((<) 9 . ind l) . concat $ indices

adj (x0, y0) (x1, y1) = (abs (x0 - x1) <= 1) && (abs (y0 - y1) <= 1) && not (x0 == x1 && y0 == y1)

ind l (x, y) = l !! y !! x

flash l p = map (map f) indices
  where
    f q =
      if adj p q
        then z + 1
        else z
      where
        z = ind l q

indices = map (\x -> map (,x) [0 .. 9]) [0 .. 9]

recFlash h l = if null f then l else recFlash h1 l1
  where
    f = sort . filter (`notElem` h) . flashing $ l
    h1 = nub $ f ++ h
    l1 = foldl flash l f

step (a, l) = (a + a1, l1)
  where
    (a1, l1) = normalize . recFlash [] . inc $ l

steps 0 = id
steps n = steps (n -1) . step

run11p1 = fst . steps 100 . (0,) . generate

syncStep l =
  if all (0 ==) . concat $ l
    then 0
    else 1 + syncStep (snd . step $ (0, l))

inp _ = "5483143223\n2745854711\n5264556173\n6141336146\n6357385478\n4167524645\n2176841721\n6882881134\n4846848554\n5283751526"

run11p2 = syncStep . generate