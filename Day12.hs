module Day12 where

import Data.Char
import Data.Map.Strict (Map, empty, filterWithKey, insertWith, (!?))
import Debug.Trace

split :: Char -> String -> [String]
split c xs = case break (== c) xs of
  (ls, "") -> [ls]
  (ls, x : rs) -> ls : split c rs

generate = foldl (\m (a, b) -> insertWith (++) b [a] . insertWith (++) a [b] $ m) empty . map strToEdge . lines

strToEdge x = (a, b)
  where
    l = split '-' x
    a = head l
    b = head . tail $ l

orElse _ (Just x) = x
orElse x Nothing = x

numPaths s l m
  | s == "end" = 1
  | s `elem` l = 0
  | otherwise = sum . map (\x -> numPaths x l2 m) . orElse [] $ m !? s
  where
    l2 = if all isLower s then s : l else l

limit "start" = Just 1
limit x
  | all isLower x = Just 2
  | otherwise = Nothing

limitReached m =
  (1 < orElse 0 (m !? "start"))
    || not
      ( null
          (filterWithKey (\k a -> all isLower k && a > 2) m)
      )
    || (1 < (length . filterWithKey (\k a -> all isLower k && a == 2) $ m))

numPaths2 s l m
  | s == "end" = 1
  | limitReached l2 = 0
  | otherwise = sum . map (\x -> numPaths2 x l2 m) . orElse [] $ m !? s
  where
    l2 = insertWith (+) s 1 l

run12p1 = numPaths "start" [] . generate

inp _ = "start-A\nstart-b\nA-c\nA-b\nb-d\nA-end\nb-end"

inp2 _ = "dc-end\nHN-start\nstart-kj\ndc-start\ndc-HN\nLN-dc\nHN-end\nkj-sa\nkj-HN\nkj-dc"

run12p2 = numPaths2 "start" empty . generate