module Day13 where

import Data.List (isPrefixOf, nub)
import Data.Tuple.Extra
import Debug.Trace

type Point = (Int, Int)

data Fold = X Int | Y Int deriving (Show)

split :: Char -> String -> [String]
split c xs = case break (== c) xs of
  (ls, "") -> [ls]
  (ls, x : rs) -> ls : split c rs

generate :: String -> ([Point], [Fold])
generate = parse . lines

disp p = unlines . map (\y -> map (\x -> if (x, y) `elem` p then '█' else '░') [0 .. sx]) $ [0 .. sy]
  where
    sx = maximum . map fst $ p
    sy = maximum . map snd $ p

strToPoint s = (head l, head . tail $ l)
  where
    l = map read . split ',' $ s

strToFold s
  | "fold along x" `isPrefixOf` s = X x
  | "fold along y" `isPrefixOf` s = Y x
  | otherwise = error ("Invalid fold " ++ s)
  where
    x = read . head . tail . split '=' $ s

parse = second parseFolds . parsePoints

parsePoints :: [String] -> ([Point], [String])
parsePoints [] = ([], [])
parsePoints (x : xs)
  | null x = ([], xs)
  | otherwise = first (\y -> strToPoint x : y) $ parsePoints xs

parseFolds :: [String] -> [Fold]
parseFolds = map strToFold

applyFold l (Y p) = nub . map (\(x, y) -> if y >= p then (x, 2 * p - y) else (x, y)) $ l
applyFold l (X p) = nub . map (\(x, y) -> if x >= p then (2 * p - x, y) else (x, y)) $ l

singleFold (p, l) = applyFold p (head l)

allFolds (p, l) = foldl applyFold p l

tr x = trace (disp x) x

trt (a, b) = trace (show b ++ "\n" ++ disp a) (a, b)

run13p1 = length . singleFold . generate

inp _ = "6,10\n0,14\n9,10\n0,3\n10,4\n4,11\n6,0\n6,12\n4,1\n0,13\n10,12\n3,4\n3,0\n8,4\n1,10\n2,14\n8,10\n9,0\n\nfold along y=7\nfold along x=5"

run13p2 = disp . allFolds . generate