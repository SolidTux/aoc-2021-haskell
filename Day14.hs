{-# LANGUAGE TupleSections #-}

module Day14 where

import Data.Map.Strict (Map, elems, empty, fromList, insertWith, toList, (!))
import qualified Data.Map.Strict as Map
import Data.Maybe
import Utils.Containers.Internal.StrictPair (toPair)

type Rule = (Char, Char, Char)

generate :: String -> (String, [Rule])
generate x = (head l, map strToRule . tail . tail $ l)
  where
    l = lines x

strToRule s = (head s, s !! 1, s !! 6)

applyRule (a0, b0) (a1, b1, x)
  | a0 == a1 && b0 == b1 = Just x
  | otherwise = Nothing

step s (x0 : x1 : xs, r) = case mapMaybe (applyRule (x0, x1)) r of
  (y : _) -> step (s ++ [x0, y]) (x1 : xs, r)
  [] -> step (s ++ [x0]) (x1 : xs, r)
step s ([x], r) = (s ++ [x], r)
step s ([], r) = (s, r)

steps 0 x = fst x
steps n x = steps (n - 1) . step "" $ x

counts s = map (\x -> length . filter (x ==) $ s) ['A' .. 'Z']

score s = maximum c - minimum c
  where
    c = filter (0 <) . counts $ s

run14p1 = score . steps 10 . generate

pair (a, b, c) = ([a, b], [[a, c], [c, b]])

toPairs (x0 : x1 : xs) = [x0, x1] : toPairs (x1 : xs)
toPairs _ = []

toMap :: String -> Map String Int
toMap = foldl (\m x -> insertWith (+) x 1 m) empty . toPairs

toRuleMap :: [Rule] -> Map String [String]
toRuleMap = fromList . map pair

tupleToMap (a, b) = ([head a, last a], toMap a, toRuleMap b)

mapStep :: Map String [String] -> Map String Int -> Map String Int
mapStep r = foldl (\m (s, n) -> insertWith (+) s n m) empty . foldl (\l (s, n) -> l ++ map (,n) (r ! s)) [] . toList

mapSteps 0 (x, m, _) = (x, m)
mapSteps n (x, m, r) = mapSteps (n - 1) (x, mapStep r m, r)

charMap :: (String, Map String Int) -> Map Char Int
charMap (s, m) = Map.map (`div` 2) . foldl (\m0 c -> insertWith (+) c 1 m0) (foldl (\m0 (x, n) -> foldl (\m1 y -> insertWith (+) y n m1) m0 x) empty . toList $ m) $ s

mapScore m = maximum v - minimum v
  where
    v = elems m

run14p2 = mapScore . charMap . mapSteps 40 . tupleToMap . generate

inp _ = "NNCB\n\nCH -> B\nHH -> N\nCB -> H\nNH -> C\nHB -> C\nHC -> B\nHN -> C\nNN -> C\nBH -> H\nNC -> B\nNB -> B\nBN -> B\nBB -> N\nBC -> B\nCC -> N\nCN -> C"