{-# LANGUAGE TupleSections #-}

module Day15 where

import Data.Char (digitToInt)
import Data.List
import Data.List.Extra (minimumOn)
import Data.Map.Strict (Map, fromList, insertWith, keys, member, restrictKeys, toList, (!), (!?))
import qualified Data.Map.Strict as Map
import Data.Maybe (mapMaybe)
import Data.Set (Set)
import qualified Data.Set as Set

generate :: String -> [[Int]]
generate = map (map digitToInt) . lines

toMap :: [[Int]] -> Map (Int, Int) Int
toMap l = fromList . concatMap (\y -> map (\x -> ((x, y), l !! y !! x)) [0 .. (length . head $ l) - 1]) $ [0 .. (length l - 1)]

neighbors m (x, y) = filter (`Set.member` m) [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]

minRisk_ :: Set (Int, Int) -> Map (Int, Int) Int -> (Int, Int) -> Map (Int, Int) Int -> Int
minRisk_ nodes dist target m
  | u == target = du
  | otherwise = minRisk_ nodes2 dist3 target m
  where
    (u, du) = minimumOn snd . toList $ dist
    nu = neighbors nodes u
    nodes2 = Set.delete u nodes
    dist2 = foldl (\x v -> insertWith min v (du + (m ! v)) x) dist nu
    dist3 = restrictKeys dist2 nodes2

minRisk m = minRisk_ (Set.fromList . keys $ m) (fromList [((0, 0), 0)]) (x, y) m
  where
    x = maximum . map fst . keys $ m
    y = maximum . map snd . keys $ m

run15p1 = minRisk . toMap . generate

enlargen :: Map (Int, Int) Int -> Map (Int, Int) Int
enlargen m = fromList . concatMap (\((x, y), v) -> map (\(ox, oy) -> ((x + w * ox, y + h * oy), wrap (v + ox + oy))) offsets) . toList $ m
  where
    offsets = concatMap (\oy -> map (,oy) [0 .. 4]) [0 .. 4]
    w = (+) 1 . maximum . map fst . keys $ m
    h = (+) 1 . maximum . map snd . keys $ m

wrap x = ((x - 1) `mod` 9) + 1

run15p2 = minRisk . enlargen . toMap . generate