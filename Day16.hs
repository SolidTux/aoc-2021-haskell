module Day16 where

import Data.Char (digitToInt)
import Data.Maybe
import Data.Tuple.Extra

data Package = Package {version :: Int, d :: PackageData} deriving (Show)

data PackageData = Literal Int | Operator {kind :: OperatorKind, packages :: [Package]} deriving (Show)

data OperatorKind = Sum | Product | Minimum | Maximum | Greater | Less | Equal deriving (Show)

parsePackage :: String -> (Maybe Package, String)
parsePackage b
  | length b < 11 = (Nothing, b)
  | otherwise = (Just Package {version = version, d = d}, r)
  where
    version = binToInt . take 3 $ b
    t = binToInt . take 3 . drop 3 $ b
    r1 = drop 6 b
    (d, r) = case t of
      0 -> parseOperator Sum r1
      1 -> parseOperator Product r1
      2 -> parseOperator Minimum r1
      3 -> parseOperator Maximum r1
      4 -> first Literal . parseLiteral $ r1
      5 -> parseOperator Greater r1
      6 -> parseOperator Less r1
      7 -> parseOperator Equal r1
      n -> error ("Invalid package type " ++ show n)

parseOperator k = first (\x -> Operator {kind = k, packages = x}) . parseSubPackages

parseSomePackages :: Maybe Int -> String -> ([Package], String)
parseSomePackages (Just 0) s = ([], s)
parseSomePackages n s = case p of
  Just x -> first (x :) . parseSomePackages n2 $ r
  Nothing -> ([], s)
  where
    (p, r) = parsePackage s
    n2 = (\x -> x - 1) <$> n

parseLiteral = first binToInt . parseLiteral_

parseLiteral_ :: String -> (String, String)
parseLiteral_ "" = error "Empty string instead of literal."
parseLiteral_ (x : b)
  | x == '1' = first (take 4 b ++) . parseLiteral_ $ drop 4 b
  | x == '0' = splitAt 4 b
parseLiteral_ s = error ("Invalid literal " ++ s)

parseSubPackages "" = error "Empty string instead of operator."
parseSubPackages (x : xs)
  | x == '1' = parseSomePackages (Just l1) (drop 11 xs)
  | x == '0' = second (\x -> x ++ drop (15 + l2) xs) . parseSomePackages Nothing . take l2 . drop 15 $xs
  where
    l1 = binToInt . take 11 $ xs
    l2 = binToInt . take 15 $ xs
parseSubPackages s = error ("Invalid operator " ++ s)

toBin "" = ""
toBin (x : xs) = charToBin x ++ toBin xs

binToInt "" = 0
binToInt x = (binToInt . init $ x) * 2 + (digitToInt . last $ x)

versionSum p = case d p of
  Literal _ -> version p
  Operator {packages = l} -> version p + (sum . map versionSum $ l)

value p = case d p of
  Literal x -> x
  Operator {kind = k, packages = pkgs} -> case k of
    Sum -> sum v
    Product -> product v
    Maximum -> maximum v
    Minimum -> minimum v
    Greater -> if v0 > v1 then 1 else 0
    Less -> if v0 < v1 then 1 else 0
    Equal -> if v0 == v1 then 1 else 0
    where
      v = map value pkgs
      v0 = head v
      v1 = head . tail $ v

charToBin '0' = "0000"
charToBin '1' = "0001"
charToBin '2' = "0010"
charToBin '3' = "0011"
charToBin '4' = "0100"
charToBin '5' = "0101"
charToBin '6' = "0110"
charToBin '7' = "0111"
charToBin '8' = "1000"
charToBin '9' = "1001"
charToBin 'A' = "1010"
charToBin 'B' = "1011"
charToBin 'C' = "1100"
charToBin 'D' = "1101"
charToBin 'E' = "1110"
charToBin 'F' = "1111"
charToBin _ = ""

generate = fromJust . fst . parsePackage . toBin

run16p1 = versionSum . generate

run16p2 = value . generate