{-# LANGUAGE TupleSections #-}

module Day17 where

import Data.List.Extra (maximumOn, sort, sortOn)
import Data.Tuple.Extra

data Result = Hit | NoHit | TooClose | TooFar deriving (Show, Eq)

type Area = ((Int, Int), (Int, Int))

candidate Hit = True
candidate NoHit = True
candidate TooClose = False
candidate TooFar = False

numString :: String -> (String, String)
numString "" = ("", "")
numString (x : xs)
  | x `elem` '-' : ['0' .. '9'] = first (x :) . numString $ xs
  | otherwise = ("", xs)

nums :: String -> [Int]
nums "" = []
nums s
  | null n = nums s2
  | otherwise = read n : nums s2
  where
    (n, s2) = numString s

generate x = ((head n, n !! 1), (n !! 2, n !! 3))
  where
    n = nums x

test f area (x, y) (vx, vy)
  | f && x > x1 = (y, TooFar)
  | y > y1 && x > x1 = (y, TooFar)
  | y < y0 && x < x0 = (y, TooClose)
  | y < y0 = (y, NoHit)
  | x >= x0 && x <= x1 && y >= y0 && y <= y1 = (y, Hit)
  | otherwise = first (max y) $ test False area (xn, yn) (vxn, vyn)
  where
    ((x0, x1), (y0, y1)) = area
    xn = x + vx
    yn = y + vy
    vxn
      | vx > 0 = vx - 1
      | vx < 0 = vx + 1
      | otherwise = vx
    vyn = vy - 1

testy a vy = takeWhile ((/=) TooFar . snd . snd) . map ((\x -> (x, test True a (0, 0) x)) . (,vy)) $ [0 .. x1]
  where
    ((x0, x1), (y0, y1)) = a

hits a = map (\(a, (b, _)) -> (a, b)) . filter ((==) Hit . snd . snd) . concat . takeWhile (not . null) . map (testy a) $ [y0 .. 2 * abs y0]
  where
    ((x0, x1), (y0, y1)) = a

calc = maximumOn snd . hits

run17p1 = calc . generate

run17p2 = length . hits . generate