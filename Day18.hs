{-# LANGUAGE TupleSections #-}

module Day18 where

import Data.Maybe
import Data.Tuple.Extra

data Element = Number Int | Pair (Element, Element) deriving (Eq)

instance Show Element where
  show (Number x) = show x
  show (Pair (x, y)) = "{" ++ show x ++ "," ++ show y ++ "}"

x +++ y = red . Pair $ (x, y)

mag (Number x) = x
mag (Pair (x, y)) = 3 * mag x + 2 * mag y

addr (Number x) y = Number (x + y)
addr (Pair (x, y)) z = Pair (x, addr y z)

addl (Number x) y = Number (x + y)
addl (Pair (x, y)) z = Pair (addl x z, y)

red x
  | e = red x2
  | s = red x3
  | otherwise = x
  where
    (x2, _, e) = explode 0 x
    (x3, s) = split x2

split (Number x)
  | x >= 10 = (Pair (Number a, Number b), True)
  | otherwise = (Number x, False)
  where
    x2 = x `div` 2
    (a, b)
      | even x = (x2, x2)
      | otherwise = (x2, x2 + 1)
split (Pair (a, b))
  | sa = (Pair (a2, b), True)
  | sb = (Pair (a, b2), True)
  | otherwise = (Pair (a, b), False)
  where
    (a2, sa) = split a
    (b2, sb) = split b

explode d (Number x) = (Number x, (0, 0), False)
explode d (Pair (Number x, Number y))
  | d == 4 = (Number 0, (x, y), True)
explode d (Pair (x, y))
  | ex = (Pair (x2, addl y rx), (lx, 0), True)
  | ey = (Pair (addr x ly, y2), (0, ry), True)
  | lx /= 0 || rx /= 0 || ly /= 0 || ry /= 0 = error "invalid"
  | otherwise = (Pair (x, y), (0, 0), False)
  where
    (x2, (lx, rx), ex) = explode (d + 1) x
    (y2, (ly, ry), ey) = explode (d + 1) y

generate :: String -> [Element]
generate = map (fromJust . fst . parseElement) . lines

close (']' : xs) = close xs
close s = s

parseElement "" = (Nothing, "")
parseElement ('[' : xs) = (p, f e2 s2)
  where
    (e, s) = parseElement xs
    (e2, s2) = parseElement s
    f (Just (Pair _)) x = tail x
    f _ x = x
    p = Just . Pair $ (fromJust e, fromJust e2)
parseElement (']' : xs) = (Nothing, xs)
parseElement (',' : xs) = parseElement xs
parseElement s = parseNum s

parseNum = first (Just . Number . read) . parseNum_

parseNum_ "" = error "Invalid number"
parseNum_ (',' : xs) = ("", xs)
parseNum_ (']' : xs) = ("", xs)
parseNum_ (x : xs) = first (x :) . parseNum_ $ xs

run18p1 = mag . red . foldl1 (+++) . generate

pairs x = map (both (x !!)) . concatMap (\a -> map (a,) . filter (a /=) $ [0 .. l]) $ [0 .. l]
  where
    l = length x - 1

run18p2 = maximum . map (mag . uncurry (+++)) . pairs . generate