{-# LANGUAGE TupleSections #-}

module Day19 where

import Control.Parallel.Strategies
import Data.List
import Data.Map.Strict (Map, elems, fromListWith)
import Data.Maybe
import Data.Tuple.Extra (both)
import Debug.Trace
import Text.Printf (printf)
import Text.Read

type Scanner = [[Int]]

type Transformation = (Offset, Rotation)

type Offset = [Int]

type Rotation = [Int]

strat = rpar

pconcatMap :: (a -> [b]) -> [a] -> [b]
pconcatMap f = concat . parMap strat f

pmap = parMap strat

split :: Char -> String -> [String]
split c xs = case break (== c) xs of
  (ls, "") -> [ls]
  (ls, x : rs) -> ls : split c rs

generate = parseScanners [] . lines

parseScanners s [] = [s]
parseScanners s (x : xs) = case parseCoordinate x of
  [] -> case s of
    [] -> parseScanners [] xs
    _ -> s : parseScanners [] xs
  l -> parseScanners (s ++ [l]) xs

parseCoordinate :: String -> [Int]
parseCoordinate = mapMaybe readMaybe . split ','

transform (o, r) = offset o . rotate r

offset o = pmap (zipWith (+) o)

rotate r = pmap (rotateCoordinate r)

rotateCoordinate [0, 0, 0] [x, y, z] = [x, y, z]
rotateCoordinate [0, 0, n] [x, y, z] = rotateCoordinate [0, 0, n - 1] [- y, x, z]
rotateCoordinate [0, n, m] [x, y, z] = rotateCoordinate [0, n - 1, m] [- z, y, x]
rotateCoordinate [n, m, o] [x, y, z] = rotateCoordinate [n - 1, m, o] [x, - z, y]
rotateCoordinate _ _ = error "Malformed rotation"

possibleTransformations :: Scanner -> Scanner -> [Transformation]
possibleTransformations a b = pconcatMap (\r -> pmap (,r) . possibleOffsets a b $ r) possibleRotations

possibleRotations :: [Rotation]
-- possibleRotations = pconcatMap (\x -> pmap (: x) [0 .. 3]) . pconcatMap (\x -> pmap (\y -> y : [x]) [0 .. 3]) $ [0 .. 3]
possibleRotations = [[0, 0, 0], [0, 0, 1], [0, 0, 2], [0, 0, 3], [0, 1, 0], [0, 1, 1], [0, 1, 2], [0, 1, 3], [0, 2, 0], [0, 2, 1], [0, 2, 2], [0, 2, 3], [0, 3, 0], [0, 3, 1], [0, 3, 2], [0, 3, 3], [1, 0, 0], [1, 0, 1], [1, 0, 2], [1, 0, 3], [1, 2, 0], [1, 2, 1], [1, 2, 2], [1, 2, 3]]

possibleOffsets :: Scanner -> Scanner -> Rotation -> [Offset]
possibleOffsets a b r = nub . pconcatMap (\x -> pmap (map negate . zipWith (-) x) a) $ br
  where
    br = rotate r b

overlap a b = length . filter (`elem` b) $ a

isValid a b t = o >= 12
  where
    o = overlap a . transform t $ b

validTransformations a b = filter (isValid a b) $ possibleTransformations a b

someValidTransformation l a = listToMaybe . pconcatMap (`validTransformations` a) . map fst $ l

align_ a b = trace (printf "%3d/%d" (length a) (length a + length b)) align' a b

align' l [] = l
align' l (x : xs) = case someValidTransformation l x of
  Just t -> align_ ((transform t x, t) : l) xs
  Nothing -> align_ l (xs ++ [x])

align [] = error "Empty scanner list"
align (x : xs) = align_ [(x, ([0, 0, 0], [0, 0, 0]))] xs

beacons :: [Scanner] -> Int
beacons = length . nub . concat

run19p1 = beacons . map fst . align . generate

pairs x = map (both (x !!)) . concatMap (\a -> map (a,) . filter (a /=) $ [0 .. l]) $ [0 .. l]
  where
    l = length x - 1

dist :: Offset -> Offset -> Int
dist a = sum . map abs . zipWith (-) a

maxDist :: [Offset] -> Int
maxDist = maximum . map (uncurry dist) . pairs

run19p2 = maxDist . map (fst . snd) . align . generate

inp _ = "--- scanner 0 ---\n404,-588,-901\n528,-643,409\n-838,591,734\n390,-675,-793\n-537,-823,-458\n-485,-357,347\n-345,-311,381\n-661,-816,-575\n-876,649,763\n-618,-824,-621\n553,345,-567\n474,580,667\n-447,-329,318\n-584,868,-557\n544,-627,-890\n564,392,-477\n455,729,728\n-892,524,684\n-689,845,-530\n423,-701,434\n7,-33,-71\n630,319,-379\n443,580,662\n-789,900,-551\n459,-707,401\n--- scanner 1 ---\n686,422,578\n605,423,415\n515,917,-361\n-336,658,858\n95,138,22\n-476,619,847\n-340,-569,-846\n567,-361,727\n-460,603,-452\n669,-402,600\n729,430,532\n-500,-761,534\n-322,571,750\n-466,-666,-811\n-429,-592,574\n-355,545,-477\n703,-491,-529\n-328,-685,520\n413,935,-424\n-391,539,-444\n586,-435,557\n-364,-763,-893\n807,-499,-711\n755,-354,-619\n553,889,-390\n--- scanner 2 ---\n649,640,665\n682,-795,504\n-784,533,-524\n-644,584,-595\n-588,-843,648\n-30,6,44\n-674,560,763\n500,723,-460\n609,671,-379\n-555,-800,653\n-675,-892,-343\n697,-426,-610\n578,704,681\n493,664,-388\n-671,-858,530\n-667,343,800\n571,-461,-707\n-138,-166,112\n-889,563,-600\n646,-828,498\n640,759,510\n-630,509,768\n-681,-892,-333\n673,-379,-804\n-742,-814,-386\n577,-820,562\n--- scanner 3 ---\n-589,542,597\n605,-692,669\n-500,565,-823\n-660,373,557\n-458,-679,-417\n-488,449,543\n-626,468,-788\n338,-750,-386\n528,-832,-391\n562,-778,733\n-938,-730,414\n543,643,-506\n-524,371,-870\n407,773,750\n-104,29,83\n378,-903,-323\n-778,-728,485\n426,699,580\n-438,-605,-362\n-469,-447,-387\n509,732,623\n647,635,-688\n-868,-804,481\n614,-800,639\n595,780,-596\n--- scanner 4 ---\n727,592,562\n-293,-554,779\n441,611,-461\n-714,465,-776\n-743,427,-804\n-660,-479,-426\n832,-632,460\n927,-485,-438\n408,393,-506\n466,436,-512\n110,16,151\n-258,-428,682\n-393,719,612\n-211,-452,876\n808,-476,-593\n-575,615,604\n-485,667,467\n-680,325,-822\n-627,-443,-432\n872,-547,-609\n833,512,582\n807,604,487\n839,-516,451\n891,-625,532\n-652,-548,-490\n30,-46,-14"

-- inp _ = "--- scanner 0 ---\n0,2,0\n4,1,0\n3,3,0\n\n--- scanner 1 ---\n-1,-1,0\n-5,0,0\n-2,1,0\n"