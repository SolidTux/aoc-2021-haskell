{-# LANGUAGE TupleSections #-}

module Day20 where

import Basement.Compat.Bifunctor (bimap)
import Data.Set (Set, fromList, member, toList)
import Day12 (orElse)

type Cell = Bool

type CellMap = Set (Int, Int)

generate :: String -> ([Cell], CellMap)
generate s = (a, toMap b)
  where
    l = lines s
    a = map charToCell . head $ l
    b = map (map charToCell) . tail . tail $ l

charToCell '#' = True
charToCell _ = False

toMap :: [[Cell]] -> CellMap
toMap g = fromList . filter (\(x, y) -> g !! y !! x) . listInds $ g

listInds g = concatMap (\x -> map (x,) [0 .. h - 1]) [0 .. w - 1]
  where
    h = length g
    w = length . head $ g

inds n g = concatMap (\x -> map (x,) [y0 .. y1]) [x0 .. x1]
  where
    x0 = (minimum . map fst . toList $ g) - n
    x1 = (maximum . map fst . toList $ g) + n
    y0 = (minimum . map snd . toList $ g) - n
    y1 = (maximum . map snd . toList $ g) + n

kernelInds (x, y) = map (bimap (x +) (y +)) . concatMap (\y -> map (,y) [-1 .. 1]) $ [-1 .. 1]

kernel :: CellMap -> (Int, Int) -> [Cell]
kernel g = map (`member` g) . kernelInds

index :: CellMap -> (Int, Int) -> Int
index g = binToInt . kernel g

cellToInt True = 1
cellToInt False = 0

binToInt :: [Cell] -> Int
binToInt [] = 0
binToInt x = 2 * (binToInt . init $ x) + (cellToInt . last $ x)

replace :: [Cell] -> CellMap -> (Int, Int) -> Cell
replace r g = (!!) r . index g

applyOnce :: Int -> ([Cell], CellMap) -> ([Cell], CellMap)
applyOnce n (r, g) = (r, g2)
  where
    n2
      | even n = 5
      | otherwise = -1
    g2 = fromList . filter (replace r g) . inds n2 $ g

apply 0 x = x
apply n x = apply (n - 1) (applyOnce n x)

lit (_, g) = length g

run20p1 = lit . apply 2 . generate

run20p2 = lit . apply 50 . generate

inp _ = "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#\n\n#..#.\n#....\n##..#\n..#..\n..###"