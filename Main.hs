module Main where

import Control.Monad
import Data.List
import Day01
import Day02
import Day03
import Day04
import Day05
import Day06
import Day07
import Day08
import Day09
import Day10
import Day11
import Day12
import Day13
import Day14
import Day15
import Day16
import Day17
import Day18
import Day19
import Day20
import System.Environment
import System.Process
import System.TimeIt

run :: Int -> Int -> String -> Maybe String
run 1 1 inp = Just . show . run01p1 $ inp
run 1 2 inp = Just . show . run01p2 $ inp
run 2 1 inp = Just . show . run02p1 $ inp
run 2 2 inp = Just . show . run02p2 $ inp
run 3 1 inp = Just . show . run03p1 $ inp
run 3 2 inp = Just . show . run03p2 $ inp
run 4 1 inp = Just . show . run04p1 $ inp
run 4 2 inp = Just . show . run04p2 $ inp
run 5 1 inp = Just . show . run05p1 $ inp
run 5 2 inp = Just . show . run05p2 $ inp
run 6 1 inp = Just . show . run06p1 $ inp
run 6 2 inp = Just . show . run06p2 $ inp
run 7 1 inp = Just . show . run07p1 $ inp
run 7 2 inp = Just . show . run07p2 $ inp
run 8 1 inp = Just . show . run08p1 $ inp
run 8 2 inp = Just . show . run08p2 $ inp
run 9 1 inp = Just . show . run09p1 $ inp
run 9 2 inp = Just . show . run09p2 $ inp
run 10 1 inp = Just . show . run10p1 $ inp
run 10 2 inp = Just . show . run10p2 $ inp
run 11 1 inp = Just . show . run11p1 $ inp
run 11 2 inp = Just . show . run11p2 $ inp
run 12 1 inp = Just . show . run12p1 $ inp
run 12 2 inp = Just . show . run12p2 $ inp
run 13 1 inp = Just . show . run13p1 $ inp
run 13 2 inp = Just . run13p2 $ inp
run 14 1 inp = Just . show . run14p1 $ inp
run 14 2 inp = Just . show . run14p2 $ inp
run 15 1 inp = Just . show . run15p1 $ inp
run 15 2 inp = Just . show . run15p2 $ inp
run 16 1 inp = Just . show . run16p1 $ inp
run 16 2 inp = Just . show . run16p2 $ inp
run 17 1 inp = Just . show . run17p1 $ inp
run 17 2 inp = Just . show . run17p2 $ inp
run 18 1 inp = Just . show . run18p1 $ inp
run 18 2 inp = Just . show . run18p2 $ inp
run 19 1 inp = Just . show . run19p1 $ inp
run 19 2 inp = Just . show . run19p2 $ inp
run 20 1 inp = Just . show . run20p1 $ inp
run 20 2 inp = Just . show . run20p2 $ inp
run d p _ = error ("day " ++ show d ++ " part " ++ show p ++ " not found")

input :: Int -> Int -> IO String
input day part = readCreateProcess (proc "nog" ["-y", "2021", "-d", show day, "-l", show part]) ""

submit day part Nothing = putStrLn ("Couldn't run day " ++ show day ++ " part " ++ show part)
submit day part (Just res) = callProcess "nog" ["-y", "2021", "-d", show day, "-l", show part, "--submit=" ++ res]

output (Just x) = putStrLn x
output Nothing = return ()

runInp day part inp = do
  let res = run day part inp
  output res

-- submit day part res

main = do
  args <- getArgs
  let day = read . head $ args :: Int
  let part = read . head . tail $ args :: Int
  inp <- input day part
  timeIt $ runInp day part inp
